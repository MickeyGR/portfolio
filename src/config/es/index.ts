import type { SiteConfig, SiteContent } from "@types";

export const SITE_CONFIG_ES: SiteConfig = {
  title: "Mickey Gudiel — Desarrollador Web y Móvil",
  author: "Mickey Gudiel Reyes",
  description:
    "Desarrollador de software con más de 4 años de experiencia creando soluciones completas, desde backend hasta frontend y aplicaciones móviles.",
  lang: "es",
  siteLogo: "/mickeygudiel-small.jpeg",
  localizedNavLinks: {
    es: [
      { text: "Experiencia", href: "#experience" },
      { text: "Proyectos", href: "#projects" },
      { text: "Sobre mí", href: "#about" },
    ],
  },
  socialLinks: [
    { text: "LinkedIn", href: "https://www.linkedin.com/in/mickey-gudiel-324446233/" },
    { text: "Facebook", href: "https://www.facebook.com/mickey.gr.77/" },
    { text: "GitLab", href: "https://gitlab.com/MickeyGR" },
  ],
  socialImage: "/mickeygudiel-og.png",
  canonicalURL: "https://mickeygudiel.com",
  privacyLink: { text: "Privacidad", href: "/es/privacy" }, // Add this
  termsLink: { text: "Términos", href: "/es/terms" }, // Add this
};

export const SITE_CONTENT_ES: SiteContent = {
  hero: {
    name: "Mickey Gudiel Reyes",
    specialty: "Desarrollador Web y Móvil",
    summary:
      "Soy un desarrollador apasionado con experiencia en tecnologías web y móviles, como Flutter, Java, NestJS y Clean Architecture. Siempre estoy buscando innovar y ofrecer soluciones de impacto.",
    email: "mickeyanthonygudiel@gmail.com",
    contactText: "Contáctame"
  },
  experience: {
    title: "Experiencia Laboral",
    seeMoreText: "Ver más experiencia",
    seeLessText: "Ver menos experiencia",
    items: [
      {
        company: "Financiera FAMA",
        position: "Analista Programador",
        startDate: "Noviembre 2022",
        endDate: "Actualidad",
        summary: [
          "Desarrollo y mantenimiento de módulos para la aplicación móvil **Mi FAMA**, disponible en Google Play y App Store.",
          "Implementación de nuevas funcionalidades utilizando Flutter y Java.",
          "Integración de servicios como Google Maps y Firebase para mejorar la experiencia del usuario.",
          "Colaboración en la implementación de Clean Architecture para garantizar soluciones escalables y robustas."
        ],
        links: [
          {
            text: "Ver en Google Play",
            href: "https://play.google.com/store/apps/details?id=ni.com.financierafama.micredito&hl=es_NI"
          },
          {
            text: "Ver en App Store",
            href: "https://apps.apple.com/ni/app/mi-fama/id1586207765"
          }
        ]
      },
      {
        company: "Comunidad Flutter",
        position: "Desarrollador de Librerías",
        startDate: "Marzo 2022",
        endDate: "Actualidad",
        summary: [
          "Diseño y desarrollo de librerías para la comunidad, publicadas en **pub.dev**.",
          "Autor de la librería **simple_biometric**, que facilita la implementación de autenticación biométrica en aplicaciones Flutter.",
          "Librería compatible con Android e iOS, con una API sencilla y personalizable.",
          "Manejo de configuraciones específicas para cada plataforma y gestión de errores comunes en autenticación biométrica."
        ],
        links: [
          {
            text: "simple_biometric en pub.dev",
            href: "https://pub.dev/packages/simple_biometric"
          },
          {
            text: "Repositorio en GitLab",
            href: "https://gitlab.com/MickeyGR/simple_biometric"
          }
        ]
      },
      {
        company: "Grupo Atenea",
        position: "Desarrollador Full Stack (Freelancer)",
        startDate: "Octubre 2022",
        endDate: "Actualidad",
        summary: [
          "Desarrollo de aplicaciones multiplataforma con Flutter.",
          "Creación de backends utilizando NestJS y Socket.io."
        ]
      },
      {
        company: "Apps-Tools",
        position: "Desarrollador Full Stack (Freelancer)",
        startDate: "Agosto 2022",
        endDate: "Octubre 2022",
        summary: [
          "Desarrollo de una aplicación móvil para la gestión de cobros y reportes diarios.",
          "Diseño e implementación de APIs utilizando NestJS y MySQL."
        ]
      },
      {
        company: "AG Software SA",
        position: "Desarrollador Full Stack",
        startDate: "Marzo 2022",
        endDate: "Septiembre 2022",
        summary: [
          "Diseño y desarrollo de software para la gestión de vacaciones.",
          "Especialización en el desarrollo de soluciones backend utilizando Java.",
          "Implementación de sistemas robustos con Apache Cocoon, garantizando escalabilidad y modularidad.",
          "Integración de tecnologías como Flutter, NestJS y PostgreSQL para aplicaciones completas."
        ]
      }
    ]
  },
  projects: [
    {
      name: "Mi FAMA",
      summary:
        "Aplicación móvil de Financiera FAMA que permite a los clientes consultar sus créditos, realizar pagos, encontrar agencias cercanas y acceder a diversos servicios financieros.",
      linkPreview: "https://play.google.com/store/apps/details?id=ni.com.financierafama.micredito&hl=es_NI",
      links: [
        {
          text: "Google Play",
          href: "https://play.google.com/store/apps/details?id=ni.com.financierafama.micredito&hl=es_NI"
        },
        { text: "App Store", href: "https://apps.apple.com/ni/app/mi-fama/id1586207765" }
      ],
      image: "/mifama.png"
    },
    {
      name: "simple_biometric",
      summary:
        "Librería en Flutter para la implementación fácil de autenticación biométrica (huella dactilar, Face ID, etc.) en Android e iOS.",
      linkPreview: "https://pub.dev/packages/simple_biometric",
      links: [
        { text: "Ver en pub.dev", href: "https://pub.dev/packages/simple_biometric" },
        { text: "Repositorio en GitLab", href: "https://gitlab.com/MickeyGR/simple_biometric" }
      ],
      image: "/dart.png"
    },
    {
      name: "marklife-label-printer-web-kit",
      summary:
        "Librería para conectar e imprimir imagenes en etiquetas como por ejemplo código de barras con impresoras Marklife desde sistemas web. Adapté un SDK exclusivo de WeChat Mini Programs para hacerlo compatible con Node.js y navegadores.",
      linkPreview: "https://www.npmjs.com/package/marklife-label-printer-web-kit",
      links: [
        { text: "Ver en npm", href: "https://www.npmjs.com/package/marklife-label-printer-web-kit" },
        { text: "Repositorio en GitLab", href: "https://gitlab.com/marklife/marklife-label-printer-web-kit" }
      ],
      image: "/marklife_p50s.png" // Agrega una imagen representativa de tu proyecto si tienes una.
    }
  ],
  about: {
    title: "Sobre mí",
    description: `
    ¡Hola! Soy Mickey Gudiel Reyes, un desarrollador apasionado por la tecnología con más de 4 años de experiencia en el desarrollo de aplicaciones móviles y sistemas web. Me especializo en Flutter, Java, NestJS y Clean Architecture, enfocándome en soluciones escalables y de alto rendimiento.

    Mi objetivo es seguir innovando y creando herramientas útiles para otros desarrolladores.
  `,
    image: "/mickeygudiel-big.jpeg",
  },
  privacy: {
    sections: [
      {
        title: "Política de Privacidad",
        href: "politica-de-privacidad", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Todas las aplicaciones desarrolladas por Mickey Gudiel, Atokatl, Atokatl.dev, o cualquiera de sus subdominios, se comprometen a proteger tu privacidad y garantizar la seguridad de tu información personal.
        </p>
      `,
      },
      {
        title: "Información que Recopilamos",
        href: "informacion-que-recopilamos", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Podemos recopilar los siguientes tipos de información:
        </p>
        <ul class="text-neutral w-auto text-base md:pr-5 list-disc pl-5">
          <li>
            Información que proporcionas directamente: Esto incluye la información que ingresas al usar nuestras aplicaciones, como tu nombre, dirección de correo electrónico u otros detalles que elijas compartir.
          </li>
          <li>
            Información de servicios de terceros (por ejemplo, Google, Amazon,
            Cloudflare): Cuando utilizas funciones que se integran con servicios de
            terceros, es posible que recibamos información de ellos. Esto puede
            incluir tokens de autenticación u otros datos personales,
            dependiendo de tu configuración de privacidad con esos servicios.
          </li>
          <li>
            Datos de uso: Recopilamos datos sobre cómo se utiliza la aplicación, lo que ayuda a mejorar nuestro servicio. Estos datos son anónimos.
          </li>
        </ul>
      `,
      },
      {
        title: "Cómo Usamos Tu Información",
        href: "como-usamos-tu-informacion", // Spanish href
        content: `
        <ul class="text-neutral w-auto text-base md:pr-5 list-disc pl-5">
          <li>Proveer y mejorar nuestras aplicaciones.</li>
          <li>Personalizar tu experiencia.</li>
          <li>Comunicarnos contigo (por ejemplo, responder a consultas).</li>
          <li>Cumplir con obligaciones legales.</li>
        </ul>
      `,
      },
      {
        title: "Seguridad de los Datos",
        href: "seguridad-de-los-datos", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Nos tomamos muy en serio la seguridad de tu información. Todos los
          datos del usuario, incluidos los tokens y la información personal
          recibida de proveedores externos como Google, Amazon y Cloudflare,
          están <strong class="font-bold">encriptados</strong> y almacenados de
          forma segura. Este cifrado hace que los datos sean ilegibles para
          personas no autorizadas. Utilizamos medidas de seguridad estándar de
          la industria para proteger contra el acceso no autorizado, la
          alteración, la divulgación o la destrucción de tus datos.
        </p>
      `,
      },
      {
        title: "Tus Derechos y Opciones",
        href: "tus-derechos-y-opciones", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Tienes derecho a acceder, corregir o eliminar tu información
          personal. Puedes solicitar la eliminación de tus datos en cualquier
          momento poniéndote en contacto con nosotros a través de los canales
          adecuados, como por ejemplo, enviando un correo electrónico a
          <a
            href="mailto:mickeyanthonygudiel@gmail.com"
            class="text-blue-500 hover:underline"
          >mickeyanthonygudiel@gmail.com</a
          >. Responderemos a tu solicitud de inmediato y de acuerdo con las
          leyes aplicables.
        </p>
      `,
      },
      {
        title: "Servicios de Terceros",
        href: "servicios-de-terceros", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Nuestras aplicaciones pueden integrarse con servicios de terceros.
          Estos servicios tienen sus propias políticas de privacidad y te
          recomendamos que las revises. No somos responsables de las
          prácticas de privacidad de estos servicios de terceros.
        </p>
      `,
      },
      {
        title: "Retención de Datos",
        href: "retencion-de-datos", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Conservamos tu información personal solo durante el tiempo necesario
          para proporcionarte nuestros servicios y según lo requiera la ley.
          Cuando los datos ya no son necesarios, los eliminamos o anonimizamos
          de forma segura.
        </p>
      `,
      },
      {
        title: "Privacidad de los Niños",
        href: "privacidad-de-los-ninos", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Nuestras aplicaciones no están dirigidas a niños menores de [13/16/etc.]. No
          recopilamos intencionalmente información personal de niños.
        </p>
      `,
      },
      {
        title: "Cambios a esta Política de Privacidad",
        href: "cambios-a-esta-politica-de-privacidad", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Podemos actualizar esta Política de Privacidad de vez en cuando. Te
          notificaremos cualquier cambio significativo publicando la nueva
          Política de Privacidad en esta página y/o a través de otros medios
          apropiados (por ejemplo, una notificación en la aplicación). Te
          recomendamos que revises esta Política de Privacidad periódicamente
          para ver si hay cambios.
        </p>
      `,
      },
      {
        title: "Contáctanos",
        href: "contactanos", // Spanish href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Si tienes alguna pregunta sobre esta Política de Privacidad, por
          favor contáctanos en:
          <a
            href="mailto:mickeyanthonygudiel@gmail.com"
            class="text-blue-500 hover:underline"
          >mickeyanthonygudiel@gmail.com</a
          >
        </p>
      `,
      },
    ],
  },
  terms: { // Add this!
    sections: [
      {
        title: "Términos de Servicio",
        href: "terminos-de-servicio",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            Estos Términos de Servicio ("Términos") rigen el uso de cualquier software, aplicación, sitio web, o servicio (colectivamente, "Servicios") desarrollado por Mickey Gudiel, Atokatl, Atokatl.dev, o cualquiera de sus subdominios. Al acceder o utilizar nuestros Servicios, aceptas estar sujeto a estos Términos.
          </p>
        `,
      },
      {
        title: "Propiedad Intelectual",
        href: "propiedad-intelectual",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            Todos los Servicios, incluyendo pero no limitado a código fuente, diseño, interfaces de usuario, logotipos, marcas registradas, y contenido, son propiedad exclusiva de Mickey Gudiel, Atokatl, o sus licenciantes, y están protegidos por las leyes de propiedad intelectual aplicables.  No se otorga ninguna licencia, derecho, o interés en los Servicios, excepto lo expresamente establecido en estos Términos.
          </p>
        `,
      },
      {
        title: "Uso Permitido",
        href: "uso-permitido",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            Se te concede un derecho limitado, no exclusivo, intransferible y revocable para acceder y utilizar los Servicios para tu uso personal o comercial, según corresponda, y de acuerdo con estos Términos.  No puedes:
            <ul class="text-neutral w-auto text-base md:pr-5 list-disc pl-5">
              <li>Modificar, copiar, distribuir, transmitir, mostrar, realizar, reproducir, publicar, licenciar, crear trabajos derivados de, transferir o vender cualquier información, software, productos o servicios obtenidos de los Servicios.</li>
              <li>Usar los Servicios para cualquier propósito ilegal o no autorizado.</li>
              <li>Intentar obtener acceso no autorizado a los Servicios o a cualquier sistema o red conectado a los Servicios.</li>
              <li>Interferir con el funcionamiento de los Servicios o con el uso de los Servicios por parte de cualquier otro usuario.</li>
              <li>Usar los Servicios de manera que viole los derechos de propiedad intelectual de terceros.</li>
              <li>Realizar ingeniería inversa, descompilar o desensamblar cualquier parte de los servicios.</li>
            </ul>
          </p>
          `,
      },
      {
        title: "Limitación de Responsabilidad",
        href: "limitacion-de-responsabilidad",
        content: `
            <p class="text-neutral w-auto text-base md:pr-5">
              En la máxima medida permitida por la ley aplicable, Mickey Gudiel y Atokatl no serán responsables de ningún daño directo, indirecto, incidental, especial, consecuente o punitivo, incluyendo, entre otros, la pérdida de ganancias, datos, uso, buena voluntad u otras pérdidas intangibles, que resulten de: (a) tu acceso o uso o incapacidad para acceder o usar los Servicios; (b) cualquier conducta o contenido de cualquier tercero en los Servicios; (c) cualquier contenido obtenido de los Servicios; y (d) acceso no autorizado, uso o alteración de tus transmisiones o contenido, ya sea basado en garantía, contrato, agravio (incluida la negligencia) o cualquier otra teoría legal, ya sea que hayamos sido informados o no de la posibilidad de tal daño, e incluso si se encuentra que un recurso establecido en este documento ha fallado en su propósito esencial.
            </p>
          `,
      },
      {
        title: "Modificaciones a los Términos",
        href: "modificaciones-terminos",
        content: `
            <p class="text-neutral w-auto text-base md:pr-5">
              Nos reservamos el derecho, a nuestra sola discreción, de modificar o reemplazar estos Términos en cualquier momento. Si una revisión es material, intentaremos proporcionar un aviso con al menos 30 días de anticipación antes de que entren en vigencia los nuevos términos. Lo que constituye un cambio material se determinará a nuestra sola discreción.
            </p>
          `,
      },
      {
        title: "Ley Aplicable",
        href: "ley-aplicable",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            Estos Términos se regirán e interpretarán de acuerdo con las leyes de Nicaragua, sin tener en cuenta sus disposiciones sobre conflictos de leyes.
          </p>
        `,
      },
      {
        title: "Contáctanos",
        href: "contactanos-terminos",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            Si tienes alguna pregunta sobre estos Términos, contáctanos en:
            <a
              href="mailto:mickeyanthonygudiel@gmail.com"
              class="text-blue-500 hover:underline"
              >mickeyanthonygudiel@gmail.com</a
            >
          </p>
        `,
      },
    ],
  },
};

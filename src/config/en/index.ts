import type { SiteConfig, SiteContent } from "@types";

export const SITE_CONFIG_EN: SiteConfig = {
  title: "Mickey Gudiel — Web and Mobile Developer",
  author: "Mickey Gudiel Reyes",
  description:
    "Software developer with over 4 years of experience creating complete solutions, from backend to frontend and mobile applications.",
  lang: "en",
  siteLogo: "/mickeygudiel-small.jpeg",
  localizedNavLinks: {
    en: [
      { text: "Experience", href: "#experience" },
      { text: "Projects", href: "#projects" },
      { text: "About Me", href: "#about" },
    ],
  },
  socialLinks: [
    { text: "LinkedIn", href: "https://www.linkedin.com/in/mickey-gudiel-324446233/" },
    { text: "Facebook", href: "https://www.facebook.com/mickey.gr.77/" },
    { text: "GitLab", href: "https://gitlab.com/MickeyGR" },
  ],
  socialImage: "/mickeygudiel-og.png",
  canonicalURL: "https://mickeygudiel.com",
  privacyLink: { text: "Privacy", href: "/en/privacy" }, // Add this
  termsLink: { text: "Terms", href: "/en/terms" }, // Add this
};

export const SITE_CONTENT_EN: SiteContent = {
  hero: {
    name: "Mickey Gudiel Reyes",
    specialty: "Web and Mobile Developer",
    summary:
      "I am a passionate developer with experience in web and mobile technologies, such as Flutter, Java, NestJS, and Clean Architecture. I am always looking to innovate and provide impactful solutions.",
    email: "mickeyanthonygudiel@gmail.com",
    contactText: "Get in Touch"
  },
  experience: {
    title: "Work Experience",
    seeMoreText: "See more experience",
    seeLessText: "See less experience",
    items: [
      {
        company: "Financiera FAMA",
        position: "Programmer Analyst",
        startDate: "November 2022",
        endDate: "Present",
        summary: [
          "Development and maintenance of modules for the **Mi FAMA** mobile application, available on Google Play and App Store.",
          "Implementation of new features using Flutter and Java.",
          "Integration of services such as Google Maps and Firebase to improve the user experience.",
          "Collaboration in the implementation of Clean Architecture to ensure scalable and robust solutions.",
        ],
        links: [
          {
            text: "View on Google Play",
            href: "https://play.google.com/store/apps/details?id=ni.com.financierafama.micredito&hl=es_NI",
          },
          {
            text: "View on App Store",
            href: "https://apps.apple.com/ni/app/mi-fama/id1586207765",
          },
        ],
      },
      {
        company: "Flutter Community",
        position: "Library Developer",
        startDate: "March 2022",
        endDate: "Present",
        summary: [
          "Design and development of libraries for the community, published on **pub.dev**.",
          "Author of the **simple_biometric** library, facilitating biometric authentication in Flutter applications.",
          "Library compatible with Android and iOS, with a simple and customizable API.",
          "Management of platform-specific configurations and common error handling in biometric authentication.",
        ],
        links: [
          {
            text: "simple_biometric on pub.dev",
            href: "https://pub.dev/packages/simple_biometric",
          },
          {
            text: "Repository on GitLab",
            href: "https://gitlab.com/MickeyGR/simple_biometric",
          },
        ],
      },
      {
        company: "Grupo Atenea",
        position: "Full Stack Developer (Freelancer)",
        startDate: "October 2022",
        endDate: "Present",
        summary: [
          "Development of cross-platform applications using Flutter.",
          "Creation of backends using NestJS and Socket.io.",
        ],
      },
      {
        company: "Apps-Tools",
        position: "Full Stack Developer (Freelancer)",
        startDate: "August 2022",
        endDate: "October 2022",
        summary: [
          "Development of a mobile application for managing collections and daily reports.",
          "Design and implementation of APIs using NestJS and MySQL.",
        ],
      },
      {
        company: "AG Software SA",
        position: "Full Stack Developer",
        startDate: "March 2022",
        endDate: "September 2022",
        summary: [
          "Design and development of vacation management software.",
          "Specialization in backend solutions using Java.",
          "Implementation of robust systems with Apache Cocoon, ensuring scalability and modularity.",
          "Integration of technologies such as Flutter, NestJS, and PostgreSQL for complete applications.",
        ],
      },
    ]
  },
  projects: [
    {
      name: "Mi FAMA",
      summary:
        "Financiera FAMA's mobile application that allows clients to check their loans, make payments, find nearby branches, and access various financial services.",
      linkPreview: "https://play.google.com/store/apps/details?id=ni.com.financierafama.micredito&hl=es_NI",
      links: [
        {
          text: "Google Play",
          href: "https://play.google.com/store/apps/details?id=ni.com.financierafama.micredito&hl=es_NI",
        },
        { text: "App Store", href: "https://apps.apple.com/ni/app/mi-fama/id1586207765" },
      ],
      image: "/mifama.png",
    },
    {
      name: "simple_biometric",
      summary:
        "Flutter library for easy implementation of biometric authentication (fingerprint, Face ID, etc.) on Android and iOS.",
      linkPreview: "https://pub.dev/packages/simple_biometric",
      links: [
        { text: "View on pub.dev", href: "https://pub.dev/packages/simple_biometric" },
        { text: "Repository on GitLab", href: "https://gitlab.com/MickeyGR/simple_biometric" },
      ],
      image: "/dart.png",
    },
    {
      name: "marklife-label-printer-web-kit",
      summary:
        "Library to connect and print images on labels (e.g., barcodes) with Marklife printers from web systems. I adapted an exclusive SDK from WeChat Mini Programs to make it compatible with Node.js and browsers.",
      linkPreview: "https://www.npmjs.com/package/marklife-label-printer-web-kit",
      links: [
        { text: "View on npm", href: "https://www.npmjs.com/package/marklife-label-printer-web-kit" },
        { text: "Repository on GitLab", href: "https://gitlab.com/marklife/marklife-label-printer-web-kit" },
      ],
      image: "/marklife_p50s.png",
    },
  ],
  about: {
    title: "About Me",
    description: `
      Hi! I am Mickey Gudiel Reyes, a passionate developer with over 4 years of experience in mobile application and web system development. I specialize in Flutter, Java, NestJS, and Clean Architecture, focusing on scalable and high-performance solutions.

      My goal is to continue innovating and creating useful tools for other developers.
    `,
    image: "/mickeygudiel-big.jpeg",
  },
  privacy: {
    sections: [
      {
        title: "Privacy Policy",
        href: "privacy-policy", // Added href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          All apps developed by Mickey Gudiel, Atokatl, Atokatl.dev, or any of
          their subdomains are committed to protecting your privacy and ensuring
          the security of your personal information.
        </p>
      `,
      },
      {
        title: "Information We Collect",
        href: "information-we-collect", // Added href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          We may collect the following types of information:
        </p>
        <ul class="text-neutral w-auto text-base md:pr-5 list-disc pl-5">
          <li>
            Information you provide directly: This includes information you enter
            when using our apps, such as your name, email address, or other
            details you choose to share.
          </li>
          <li>
            Information from third-party services (e.g., Google, Amazon,
            Cloudflare): When you use features that integrate with third-party
            services, we may receive information from them. This may include
            authentication tokens or other personal data, depending on your
            privacy settings with those services.
          </li>
          <li>
            Usage Data: We collect data on how the application is being used, this
            helps make our service better. This data is anonymized.
          </li>
        </ul>
      `,
      },
      {
        title: "How We Use Your Information",
        href: "how-we-use-information", // Added href
        content: `
        <ul class="text-neutral w-auto text-base md:pr-5 list-disc pl-5">
          <li>Provide and improve our apps.</li>
          <li>Personalize your experience.</li>
          <li>Communicate with you (e.g., respond to inquiries).</li>
          <li>Fulfill legal obligations.</li>
        </ul>
      `,
      },
      {
        title: "Data Security",
        href: "data-security", // Added href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          We take the security of your information very seriously. All user data,
          including tokens and personal information received from external
          providers like Google, Amazon, and Cloudflare, is
          <strong class="font-bold">encrypted</strong> and stored securely. This
          encryption makes the data unreadable to unauthorized parties. We use
          industry-standard security measures to protect against unauthorized
          access, alteration, disclosure, or destruction of your data.
        </p>
      `,
      },
      {
        title: "Your Rights and Choices",
        href: "your-rights-and-choices", // Added href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          You have the right to access, correct, or delete your personal
          information.  You can request the deletion of your data at any time by
          contacting us through the proper channels, such as by emailing
          <a
            href="mailto:mickeyanthonygudiel@gmail.com"
            class="text-blue-500 hover:underline"
          >mickeyanthonygudiel@gmail.com</a
          >. We will respond to your request promptly and in accordance with
          applicable laws.
        </p>
      `,
      },
      {
        title: "Third-Party Services",
        href: "third-party-services", // Added href
        content: `
        <p class="text-neutral w-auto text-base md:pr-5">
          Our apps may integrate with third-party services. These services have
          their own privacy policies, and we encourage you to review them. We are
          not responsible for the privacy practices of these third-party services.
        </p>
      `,
      },
      {
        title: "Data Retention",
        href: "data-retention", // Added href
        content: `
      <p class="text-neutral w-auto text-base md:pr-5">
      We retain your personal information only for as long as necessary to provide you with our services and as required by law. When data is no longer needed, we securely delete or anonymize it.
      </p>
  `,
      },
      {
        title: "Children's Privacy",
        href: "childrens-privacy", // Added href
        content: `
    <p class="text-neutral w-auto text-base md:pr-5">
      Our apps are not intended for children under the age of [13/16/etc.]. We
      do not knowingly collect personal information from children.
    </p>
  `,
      },
      {

        title: "Changes to this Privacy Policy",
        href: "changes-to-privacy-policy", // Added href
        content: `<p class="text-neutral w-auto text-base md:pr-5">
      We may update this Privacy Policy from time to time. We will notify you
      of any significant changes by posting the new Privacy Policy on this
      page and/or through other appropriate means (e.g., in-app notification).
      You are advised to review this Privacy Policy periodically for any
      changes.
    </p>`,
      },
      {
        title: "Contact Us",
        href: "contact-us", // Added href
        content: `<p class="text-neutral w-auto text-base md:pr-5">
      If you have any questions about this Privacy Policy, please contact us
      at:
      <a
        href="mailto:mickeyanthonygudiel@gmail.com"
        class="text-blue-500 hover:underline"
      >mickeyanthonygudiel@gmail.com</a
      >
    </p>`,
      },
    ],
  },
  terms: { // Add this
    sections: [
      {
        title: "Terms of Service",
        href: "terms-of-service",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            These Terms of Service ("Terms") govern your use of any software, application, website, or service (collectively, "Services") developed by Mickey Gudiel, Atokatl, Atokatl.dev, or any of their subdomains. By accessing or using our Services, you agree to be bound by these Terms.
          </p>
        `,
      },
      {
        title: "Intellectual Property",
        href: "intellectual-property",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            All Services, including but not limited to source code, design, user interfaces, logos, trademarks, and content, are the exclusive property of Mickey Gudiel, Atokatl, or their licensors, and are protected by applicable intellectual property laws. No license, right, or interest in the Services is granted, except as expressly stated in these Terms.
          </p>
        `,
      },
      {
        title: "Permitted Use",
        href: "permitted-use",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            You are granted a limited, non-exclusive, non-transferable, and revocable right to access and use the Services for your personal or commercial use, as applicable, and in accordance with these Terms. You may not:
            <ul class="text-neutral w-auto text-base md:pr-5 list-disc pl-5">
              <li>Modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, software, products, or services obtained from the Services.</li>
              <li>Use the Services for any illegal or unauthorized purpose.</li>
              <li>Attempt to gain unauthorized access to the Services or any systems or networks connected to the Services.</li>
              <li>Interfere with the operation of the Services or with any other user's use of the Services.</li>
              <li>Use the Services in a manner that infringes the intellectual property rights of third parties.</li>
              <li>Reverse engineer, decompile, or disassemble any portion of the services.</li>
            </ul>
          </p>
        `,
      },
      {
        title: "Limitation of Liability",
        href: "limitation-of-liability",
        content: `
            <p class="text-neutral w-auto text-base md:pr-5">
              To the maximum extent permitted by applicable law, Mickey Gudiel and Atokatl shall not be liable for any direct, indirect, incidental, special, consequential, or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from: (a) your access to or use of or inability to access or use the Services; (b) any conduct or content of any third party on the Services; (c) any content obtained from the Services; and (d) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.
            </p>
          `,
      },
      {
        title: "Modifications to the Terms",
        href: "modifications-terms",
        content: `
            <p class="text-neutral w-auto text-base md:pr-5">
              We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
            </p>
          `,
      },
      {
        title: "Governing Law",
        href: "governing-law",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            These Terms shall be governed and construed in accordance with the laws of Nicaragua, without regard to its conflict of law provisions.
          </p>
        `,
      },
      {
        title: "Contact Us",
        href: "contact-us-terms",
        content: `
          <p class="text-neutral w-auto text-base md:pr-5">
            If you have any questions about these Terms, please contact us at:
            <a
              href="mailto:mickeyanthonygudiel@gmail.com"
              class="text-blue-500 hover:underline"
              >mickeyanthonygudiel@gmail.com</a
            >
          </p>
        `,
      },
    ],
  },
};

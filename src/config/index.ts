import { SITE_CONFIG_ES, SITE_CONTENT_ES } from "./es/index";
import { SITE_CONFIG_EN, SITE_CONTENT_EN } from "./en/index";
import type { SiteConfig, SiteContent } from "../types";

// Configuraciones por idioma
export const SITE_CONFIG: Record<string, SiteConfig> = {
  es: SITE_CONFIG_ES,
  en: SITE_CONFIG_EN,
};

// Contenido por idioma
export const SITE_CONTENTS: Record<string, SiteContent> = {
  es: SITE_CONTENT_ES,
  en: SITE_CONTENT_EN,
};

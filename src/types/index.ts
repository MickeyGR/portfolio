export interface SiteConfig extends HeaderProps {
  title: string;
  description: string;
  lang: string;
  author: string;
  socialLinks: { text: string; href: string }[];
  socialImage: string;
  canonicalURL?: string;
  localizedNavLinks: Record<string, { text: string; href: string }[]>; // Enlaces de navegación localizados por idioma
  privacyLink: { text: string; href: string };
  termsLink: { text: string; href: string }; // Add this!
}

export interface SiteContent {
  hero: HeroProps;
  experience: ExperienceProps;
  projects: ProjectProps[];
  about: AboutProps;
  privacy: {
    sections: { title: string; href: string; content: string }[];
  };
  terms: {  // Add this!
    sections: { title: string; content: string; href: string }[];
  };
}

export interface HeroProps {
  name: string;
  specialty: string;
  summary: string;
  email: string;
  contactText: string;
}

export interface ExperienceProps {
  title: string; // Título de la sección
  seeMoreText: string; // Texto para expandir
  seeLessText: string; // Texto para colapsar
  items: ExperienceItemProps[]; // Lista de experiencias
}

export interface ExperienceItemProps {
  company: string;
  position: string;
  startDate: string;
  endDate: string;
  summary: string | string[];
  links?: { text: string; href: string }[]; // Enlaces opcionales
}

export interface ProjectProps {
  name: string;
  summary: string;
  image: string;
  linkPreview?: string;
  linkSource?: string;
  links?: { text: string; href: string }[]; // Agregar links opcionales
}

export interface AboutProps {
  title: string;
  description: string;
  image: string;
}

export interface HeaderProps {
  siteLogo: string;
  localizedNavLinks: Record<string, { text: string; href: string }[]>; // Enlaces de navegación localizados por idioma
  lang: string; // Idioma actual
}

// @ts-check
import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  integrations: [tailwind()],
  base: "/",
  // site: "https://mickeygr.gitlab.io/portfolio/", // URL completa de tu página
  i18n: {
    locales: ["es", "en"], // Idiomas soportados
    defaultLocale: "es", // Idioma por defecto
    routing: {
      prefixDefaultLocale: false, // No usar prefijo de idioma para el idioma por defecto
      redirectToDefaultLocale: true, // Redirigir a la página por defecto cuando la ruta sea "/"
    },
  },
});
